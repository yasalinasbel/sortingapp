package org.epamtest;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class AppTest {
    private String[] array;
    private int[] expected;
    private App app;


    public AppTest(String[] array ){
        this.array=array;
    }
    @Before
    public void setUp() {
        app = new App();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new String[]{}},
                {new String[]{"50","3","7","8","25","16","9","7","8","9","10","1","30","15","7","6","17"}},
                {new String[]{"50","3","7","8","25","16","9","7","3","7","8","25","16","9","7"}},
                {new String[]{"25","16","9","7","8","9","10","1","25","16","9","7","8","9","10","1","25","16","9","7","8","9","10","1","25","16","9","7","8","9","10","1"}},
                {new String[]{"50","3","7","8","25","16","9","7","8","9","10","1","30","15","7","6","17","8","9","10","1","30","15","7","6","17"}},
        });
    }
    @Test(expected = IllegalArgumentException.class)
    public void sort() {
        app.main(array);
    }
}