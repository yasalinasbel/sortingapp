package org.epamtest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * The App class is responsible for receiving ten numbers from the user that will be ordered in ascending order.
 *
 */
public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main( String[] args ) {
        if(args.length>10 || args.length==0){
            throw new IllegalArgumentException("Enter a number of arguments greater than or equal to 1 and less than or equal to 10");
        }
        Sorting sorting=new Sorting();
        int[] argsInt = transformToInt(args);
        sorting.sort(argsInt);
        for(int arg:argsInt){
            logger.info(arg);
        }
    }

    private static int[] transformToInt(String[] args) {
        int[] argsInt=new int[args.length];
        for(int i = 0; i< args.length; i++){
            argsInt[i]=Integer.parseInt(args[i]);
        }
        return argsInt;
    }

}
