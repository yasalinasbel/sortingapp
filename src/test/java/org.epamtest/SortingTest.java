package org.epamtest;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runners.*;

import java.util.*;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTest {
    private int[] array;
    private int[] expected;
    private Sorting sorting;


    public SortingTest(int[] array, int[]expected ){
        this.array=array;
        this.expected=expected;
    }
    @Before
    public void setUp() {
        sorting = new Sorting();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {new int[]{22},new int[]{22}},
                {new int[]{50,3,7,8,25,16,9},new int[]{3,7,8,9,16,25,50}},
                {new int[]{7,8,9,10,1,30,15,7,6,17},new int[]{1,6,7,7,8,9,10,15,17,30}},
                {new int[]{1,2,3,4,5,6,7,8,9,10,11,12},new int[]{1,2,3,4,5,6,7,8,9,10,11,12}},
                {new int[]{1,2,3,4,5,6,7,0,9,10,11,12},new int[]{0,1,2,3,4,5,6,7,9,10,11,12}},
                {new int[]{0,0,0,0,0,0,0,0,0,0},new int[]{0,0,0,0,0,0,0,0,0,0}},
                {new int[]{-1,2,3,-4,5,6,-7,8,9,-10,11,-12},new int[]{-12,-10,-7,-4,-1,2,3,5,6,8,9,11}},
                {new int[]{-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12},new int[]{-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1}},
        });
    }
    @Test
    public void sort() {
        sorting.sort(array);
        assertArrayEquals(expected,array);
    }
}
