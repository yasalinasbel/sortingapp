# SortingApp | Java | Back-end |

-This application is designed to arrange 10 numbers in ascending order that the user enters.
If the user enters  numbers to be sorted greater than 10, the system will throw an error.


---
##  To execute de app:


#### 🔹 Write in console:</br> 
 Mvn clean package ⭐
#### 🔹 Write in console: </br> 

 java -jar target/SortingApp.jar number1 number2 number3 number4 number5 number6 number7 number8 number9 number10 📚

</br>

## 🖥️ Used Technology:

- Java 8
- IntelliJ IDEA
- Maven 3
- JUnit 4
- Log4j </br>

---
## ⚠️ Important! ⚠️

☕ Use Java version 8 for compatibility. </br></br>

